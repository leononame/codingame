#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

struct defibrillator {
	double longitude;
	double latitude;
	char name[100];
};

struct defibrillator parse_defibrillator(char* str);
double calc_distance(struct defibrillator d, double user_long, double user_lat);

int main()
{
	// delimiter
	char del = ';';
	double user_lat;
	double user_long;

	// read user latitude, longitude and n defibrillators
	char u_la[51];
	char u_lo[51];
	int n;
	scanf("%s", u_lo);
	scanf("%s", u_la);
	scanf("%d", &n); fgetc(stdin);
	// parse longitude, latitude
	// replace , with .
	char* p;
	p = strchr(u_la, ',');
	*p = '.';
	p = strchr(u_lo, ',');
	*p = '.';
	user_long = strtod(u_lo, NULL);
	user_lat = strtod(u_la, NULL);

	double max_distance = 5000000.0;
	char shortest[100];
	// Loop through n
	while(n--)
	{
		// get data
		char current[257];
		fgets(current, 257, stdin);

		struct defibrillator d = parse_defibrillator(current);
		double distance = calc_distance(d, user_long, user_lat);
		if (distance < max_distance)
		{
		    max_distance = distance;
		    strcpy(shortest, d.name);
		}

	}
	printf("%s\n", shortest);
	return 0;
}

/*
 * Parse string to defibrillator struct
 */
struct defibrillator parse_defibrillator(char* str)
{
	struct defibrillator d;
	char del = ';';

	char* p = strchr(str, del);
	p++;
	// get name
	char* name = p;

	// ignore addr, phone
	p = strchr(p, del);
	*p = '\0'; // set to 0 to finish char string
	strcpy(d.name, name);
	p++;
	p = strchr(p, del);
	p++;
	p = strchr(p, del);
	p++;

	// get lat, long
    char* los = p;
    p = strchr(p, del);
    *p = '\0';
    char* las = ++p;
    p = strchr(p, '\n');
    *p = '\0';

	// replace , with .
	p = strchr(los, ',');
	*p = '.';
	p = strchr(las, ',');
	*p = '.';
	d.longitude = strtod(los, NULL);
	d.latitude = strtod(las, NULL);
	return d;
}

double calc_distance(struct defibrillator d, double user_long, double user_lat)
{
	double diff_lo = d.longitude - user_long;
	double y = d.latitude - user_lat;
	double x = diff_lo * cos( (d.latitude + user_lat) / 2.0);
	return sqrt(y*y + x*x) * 6371;
}
