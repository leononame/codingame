#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int surfaceN; // the number of points used to draw the surface of Mars.
    scanf("%d", &surfaceN);
    for (int i = 0; i < surfaceN; i++) {
        int landX; // X coordinate of a surface point. (0 to 6999)
        int landY; // Y coordinate of a surface point. By linking all the points together in a sequential fashion, you form the surface of Mars.
        scanf("%d%d", &landX, &landY);
    }

    // game loop
    while (1) {
      int x;
        int y;
        int vx; // the horizontal speed (in m/s), can be negative.
        int vy; // the vertical speed (in m/s), can be negative.
        int fuel; // the quantity of remaining fuel in liters.
        int rot; // the rotation angle in degrees (-90 to 90).
        int p; // the thrust power (0 to 4).
        scanf("%d%d%d%d%d%d%d", &x, &y, &vx, &vy, &fuel, &rot, &p);
        // Write an action using printf(). DON'T FORGET THE TRAILING \n
        // To debug: fprintf(stderr, "Debug messages...\n");

        if (vy < -38)
            printf("0 4\n");
        else
            printf("0 0\n");
        
        // 2 integers: rotate power. rotate is the desired rotation angle (should be 0 for level 1), power is the desired thrust power (0 to 4).
     
    }

    return 0;
}
