#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
	int n; // the number of temperatures to analyse
	scanf("%d", &n);

	int d=5526, v=0;
	for (int i = 0; i < n; i++) {
		int t; // a temperature expressed as an integer ranging from -273 to 5526
		scanf("%d", &t);
		if (abs(t) < d || (abs(t) == d) && v < t)
		{
			d = abs(t);
			v = t;
		}
	}

	// Write an action using printf(). DON'T FORGET THE TRAILING \n
	// To debug: fprintf(stderr, "Debug messages...\n");

	printf("%d\n", v);

	return 0;
}

