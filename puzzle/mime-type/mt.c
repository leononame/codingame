#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * Define a hastable for lookuo
 */

struct nlist { /* table entry: */
	struct nlist *next; /* next entry in chain */
	char *name; /* defined name */
	char *defn; /* replacement text */
};

#define HASHSIZE 101
static struct nlist *hashtab[HASHSIZE]; /* pointer table */

/* hash: form hash value for string s */
unsigned hash(char *s)
{
	unsigned hashval;
	for (hashval = 0; *s != '\0'; s++)
		hashval = *s + 31 * hashval;
	return hashval % HASHSIZE;
}

/* lookup: look for s in hashtab */
struct nlist *lookup(char *s)
{
	struct nlist *np;
	for (np = hashtab[hash(s)]; np != NULL; np = np->next)
		if (strcmp(s, np->name) == 0)
			return np; /* found */
	return NULL; /* not found */
}

char *strdup(char *);
/* install: put (name, defn) in hashtab */
struct nlist *install(char *name, char *defn)
{
	struct nlist *np;
	unsigned hashval;
	if ((np = lookup(name)) == NULL) { /* not found */
		np = (struct nlist *) malloc(sizeof(*np));
		if (np == NULL || (np->name = strdup(name)) == NULL)
			return NULL;
		hashval = hash(name);
		np->next = hashtab[hashval];
		hashtab[hashval] = np;
	} else /* already there */
		free((void *) np->defn); /*free previous defn */
	if ((np->defn = strdup(defn)) == NULL)
		return NULL;
	return np;
}

char *strdup(char *s) /* make a duplicate of s */
{
	char *p;
	p = (char *) malloc(strlen(s)+1); /* +1 for ’\0’ */
	if (p != NULL)
		strcpy(p, s);
	return p;
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
	int N; // Number of elements which make up the association table.
	scanf("%d", &N);
	int Q; // Number Q of file names to be analyzed.
	scanf("%d", &Q);

	// Read data
	for (int i = 0; i < N; i++) {
		char EXT[101]; // file extension
		char MT[501]; // MIME type.
		scanf("%s%s", EXT, MT); fgetc(stdin);
		// Lowercase
		char* p = EXT;
		for(;*p;p++) *p = tolower(*p);
		// Add to hash table
		install(EXT, MT);
	}

	// Read challenges
	for (int i = 0; i < Q; i++) {
		char FNAME[501]; // One file name per line.
		fgets(FNAME, 501, stdin); // One file name per line.
		
		// Remove trailing newline
		char* p = strchr(FNAME, '\n');
		if (p != NULL) *p=0;
		
		// Make lower case
		p = FNAME;
		for(;*p;p++) *p = tolower(*p);

		// Get extension
		char* ext = strrchr(FNAME, '.');
		if (ext == NULL)
		{
			printf("UNKNOWN\n");
			continue;
		}
		// Lookup extension
		struct nlist *el = lookup(++ext);
		if(el == NULL)
			printf("UNKNOWN\n");
		else
			printf("%s\n", el->defn);
	}
	return 0;
}
