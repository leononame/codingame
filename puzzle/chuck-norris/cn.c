#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int curr = (MESSAGE[0] >> 6) & 0x01;
    char* control_char = "00";
    char MESSAGE[101];
    fgets(MESSAGE, 101, stdin);
    
    printf("%s ", &control_char[curr]);
    for (char *c = MESSAGE; *c && c[0] != '\n'; c++)
    {     
        for (int i = 6; i >=0; i--)
        {
            if (((*c >> i) & 0x01) != curr)
            {
                curr ^= 0x01;
                printf(" %s ", &control_char[curr]);
            }
            printf("0");
        }
    }
    printf("\n");
    return 0;
}
